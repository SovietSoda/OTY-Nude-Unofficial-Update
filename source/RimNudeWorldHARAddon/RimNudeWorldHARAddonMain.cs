﻿using System.Linq;
using RimWorld;
using HarmonyLib;
using Verse;
using UnityEngine;
using AlienRace;
using rjw;

namespace RimNudeWorldHARAddon
{
	[StaticConstructorOnStartup]
	public class RimNudeWorldHARAddonMain
	{
		public static bool useCumflation;

		static RimNudeWorldHARAddonMain()
		{
			Harmony val = new Harmony("RimNudeWorld_HAR_Addon");
			Log.Message("RimNudeWorldHARAddon Patch");
			useCumflation = (LoadedModManager.RunningModsListForReading.Any(x => x.PackageId == "LustLicentia.RJWLabs"));
			if (!useCumflation)
			{
				if (HediffDef.Named("Cumflation") != null)
					useCumflation = true;
			}
				
			if (useCumflation)	Log.Message("RimNudeWorldHARAddon: UseCumflation");
			val.PatchAll();
		}

		//[HarmonyPatch(typeof(SexUtility), "DrawNude")]
		class DrawNudePatch
		{
			static bool Prefix(Pawn pawn)
			{
				Log.Warning("DrawNudePrefix");
				if (!xxx.is_human(pawn))
					return false;
				AlienPartGenerator.AlienComp alienComp = pawn.GetComp<AlienPartGenerator.AlienComp>();
				if (alienComp != null)
				{
					for (int i = 0; i < alienComp.addonGraphics.Count; i++)
					{
						Graphic agr = alienComp.addonGraphics[i];
						//Log.Message(agr.path);
						HARPatch.Postfix(pawn, ref agr);
						//Log.Message(agr.path);
					}
				}
				return true;
				

				//pawn.Drawer.renderer.graphics.SetAllGraphicsDirty();
				//partner.Drawer.renderer.graphics.SetAllGraphicsDirty();
			}
		}

		[HarmonyPatch(typeof(JobDriver_SexBaseInitiator), "Start")]
		class SetTextureDuringSet
		{
			static void Prefix(JobDriver_SexBaseInitiator __instance)
			{
				//Log.Warning("SexStart");

				var partner = __instance.Partner as Pawn;
				/*
				
				for(int i = 0; i< pawn.Drawer.renderer.graphics.apparelGraphics.Count; i++)
				{
					ApparelGraphicRecord agr = pawn.Drawer.renderer.graphics.apparelGraphics[i];
					Log.Message(agr.graphic.path);
					HARPatch.Postfix(pawn, ref agr.graphic);
				}
				*/
				//pawn.Drawer.renderer.graphics.ClearCache();
				//partner.Drawer.renderer.graphics.ClearCache();
				//__instance.pawn.Drawer.renderer.graphics.SetAllGraphicsDirty();
				__instance.pawn.Drawer.renderer.graphics.ResolveAllGraphics();
				AlienPartGenerator.AlienComp alienComp = __instance.pawn.GetComp<AlienPartGenerator.AlienComp>();
				if(alienComp != null)
				{
					for (int i = 0; i < alienComp.addonGraphics.Count; i++)
					{
						Graphic agr = alienComp.addonGraphics[i];
						//Log.Message(agr.path);
						HARPatch.Postfix(__instance.pawn, ref agr);
						//Log.Message(agr.path);
					}
				}
				if (partner != null)
				{
					//partner.Drawer.renderer.graphics.SetAllGraphicsDirty();
					//partner.Drawer.renderer.graphics.ResolveApparelGraphics();
					partner.Drawer.renderer.graphics.ResolveAllGraphics();
					alienComp = partner.GetComp<AlienPartGenerator.AlienComp>();
					if (alienComp != null)
					{
						for (int i = 0; i < alienComp.addonGraphics.Count; i++)
						{
							Graphic agr = alienComp.addonGraphics[i];
							//Log.Message(agr.path);
							HARPatch.Postfix(partner, ref agr);
							//Log.Message(agr.path);
							
						}
					}
				}

				//pawn.Drawer.renderer.graphics.SetAllGraphicsDirty();
				//partner.Drawer.renderer.graphics.SetAllGraphicsDirty();
			}
		}

		public static bool IsLactating(Pawn pawn)
		{
			if(pawn != null)
			{
				if (pawn.RaceProps.Humanlike)
				{
					foreach (Hediff hediff in pawn.health.hediffSet.hediffs)
					{
						if (hediff != null)
						{
							if (hediff.def.defName.Contains("Lactating") || hediff.def.defName.Contains("lactating"))
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		/*
		[HarmonyPatch(typeof(PawnGraphicSet), "ResolveAllGraphics")]
		class Test
		{
			static void Postfix(ref PawnGraphicSet __instance)
			{
				if (__instance.pawn.RaceProps.Humanlike)
				{
					var dri = __instance.pawn.jobs.curDriver as rjw.JobDriver_Sex;

					if (dri != null)
					{
						Log.Warning("ResolveGraphicDuringSex");
					}
				}
			}

		}*/
		public class TestClass
		{

			public void Testfunc1()
			{
				string text = "Hello";
				text.Contains("lo");
			}
			public void Testfunc2()
			{
				float a = 1;
				float b = 2;
				float c = 3;
				if ((a > c ? a : c) > b)
				{

				}
			}
		}

		[HarmonyPatch(typeof(AlienPartGenerator.BodyAddon), "GetPath")]
		class HARPatch
		{
			/*
			public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
			{
				
				
				FieldInfo pathInfo = AccessTools.Field(typeof(AlienPartGenerator.BodyAddon), "path");
				int num;
				CodeInstruction targetPawnInstruction;
				List<CodeInstruction> instructionList = instructions.ToList<CodeInstruction>();
				//Hediff cumflation = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("Cumflation"));
				for (int i = 0; i < instructionList.Count; i = num + 1)
				{
					CodeInstruction codeInstruction = instructionList[i];
					
					num = i;

					if (codeInstruction.opcode == OpCodes.Ldfld)
					{
						if (codeInstruction.OperandIs(pathInfo))
						{
							if(instructionList[i-2].opcode == OpCodes.Ldloc_S)
							{
								//targetPawn;
								targetPawnInstruction = instructionList[i - 2];
							}
						}
					}

					if (codeInstruction.opcode == OpCodes.Ldloc_S)
					{
						
					}

					if (codeInstruction.opcode == OpCodes.Callvirt)
					{
						//Target Hediff
						instructionList[i - 1];
					}

					if (codeInstruction.opcode == OpCodes.Blt_Un_S)
					{
						//yield return new CodeInstruction(OpCodes.)
						
						yield return codeInstruction;
					}

					else
					yield return codeInstruction;
				}
				instructionList = null;
				yield break;

			}*/

			public static void Postfix(Pawn pawn, ref Graphic __result)
			{
				if(__result != null)
				{
					if (pawn == null)
						return;
					string originalPath = __result.path;
					bool validTexture = false;

					//Body typed texture
					if (pawn.story.bodyType == BodyTypeDefOf.Hulk || pawn.story.bodyType == BodyTypeDefOf.Fat)
					{
						if (pawn.story.bodyType == BodyTypeDefOf.Hulk)
						{
							if ((ContentFinder<Texture2D>.Get(originalPath + "_Hulk" + "_south", false) != null))
							{
								Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Hulk", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
								__result = newGraphic;
								validTexture = true;
							}
						}
						else if (pawn.story.bodyType == BodyTypeDefOf.Fat)
						{
							if ((ContentFinder<Texture2D>.Get(originalPath + "_Fat" + "_south", false) != null))
							{
								Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Fat", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
								__result = newGraphic;
								validTexture = true;
							}
						}
						if (validTexture == false)
						{
							if ((ContentFinder<Texture2D>.Get(originalPath + "_Wide" + "_south", false) != null))
							{
								Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Wide", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
								__result = newGraphic;
								validTexture = true;
							}
						}
					}
					else if (pawn.story.bodyType == BodyTypeDefOf.Thin)
					{
						if ((ContentFinder<Texture2D>.Get(originalPath + "_Thin" + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Thin", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;
							validTexture = true;
						}
					}
					else if (pawn.story.bodyType == BodyTypeDefOf.Male)
					{
						if ((ContentFinder<Texture2D>.Get(originalPath + "_Male" + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Male", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;
							validTexture = true;
						}
					}
					else if (pawn.story.bodyType == BodyTypeDefOf.Female)
					{
						if ((ContentFinder<Texture2D>.Get(originalPath + "_Female" + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_Female", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;
							validTexture = true;
						}
					}
					else
					{
						string bodyname = pawn.story.bodyType.defName;
						if ((ContentFinder<Texture2D>.Get(originalPath + "_" + bodyname + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(originalPath + "_" + bodyname, __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;
							validTexture = true;
						}
					}


					bool erect = false;

					Need_Sex needSex = null;
					if (pawn.needs != null)
						needSex = pawn.needs.TryGetNeed<Need_Sex>();

					JobDriver_SexBaseInitiator dri = null;
					if(pawn.jobs != null)
						dri = pawn.jobs.curDriver as rjw.JobDriver_SexBaseInitiator;


					//Log.Message("find needSex");
					if (needSex != null)
					{
						//Log.Warning(needSex.CurLevel.ToString());
						if (needSex.CurLevel >= needSex.thresh_ahegao() || needSex.CurLevel < needSex.thresh_neutral())
						{
							erect = true;
						}
					}
					else
					{
						//Log.Warning("SexNeeds Not Found");
					}


					//Log.Message("find jobdriver_sex");
					if (dri != null)
					{
						erect = true;
					}
					else
					{
						//Log.Warning("JobDriver_Sex not found");
					}


					if (erect)
					{
						//Log.Warning("finding Erect Texture");
						if ((ContentFinder<Texture2D>.Get(__result.path + "_Erect" + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(__result.path + "_Erect", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;

						}
					}


					//lactation
					if (IsLactating(pawn))
					{
						//Log.Message("finding Lactation Texture");
						if ((ContentFinder<Texture2D>.Get(__result.path + "_Lactating" + "_south", false) != null))
						{
							Graphic newGraphic = GraphicDatabase.Get<Graphic_Multi>(__result.path + "_Lactating", __result.Shader, __result.drawSize, __result.color, __result.colorTwo);
							__result = newGraphic;

						}
					}
				}
			}
		}
	}
}