﻿using System;
using System.Linq;
using HarmonyLib;
using Verse;
using rjw;

namespace RimNudeWorldHARAddon
{
	public class BellyBulge : HediffWithComps
	{
		Hediff cumflation = null;
		Hediff pregnancy = null;
		public override bool Visible => false;

		public override void PostMake()
		{

			base.PostMake();
		}

		public override void ExposeData()
		{
			base.ExposeData();
		}
		public override void Tick()
		{
			base.Tick();

			if (!pawn.IsHashIntervalTick(60))
				return;

			if(RimNudeWorldHARAddonMain.useCumflation)
				cumflation = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("Cumflation"));

			pregnancy = pawn.health.hediffSet.hediffs.FirstOrDefault(x => x is Hediff_BasePregnancy);

			this.Severity = (cumflation != null ? cumflation.Severity : 0) + (pregnancy != null ? pregnancy.Severity : 0);

			if (this.Severity == 0)
			{
				if(pregnancy == null)
					pawn.health?.RemoveHediff(this);
			}
		}
	}


	[HarmonyPatch(typeof(SexUtility), "Aftersex")]
	[HarmonyPatch(new Type[] { typeof(Pawn), typeof(Pawn), typeof(bool), typeof(bool), typeof(bool), typeof(xxx.rjwSextype) })]
	public class BellyHeddiffAddPatch
	{
		public static void Postfix(Pawn pawn, Pawn partner)
		{
			if (pawn.health.hediffSet.HasHediff(HediffDef.Named("RNW_BellyBulge")))
			{
				pawn.Drawer.renderer.graphics.ResolveAllGraphics();
				//pawn.Drawer.renderer.graphics.ClearCache();
			}
			else
			{
				if (pawn.IsVisiblyPregnant() || RimNudeWorldHARAddonMain.useCumflation? pawn.health.hediffSet.HasHediff(HediffDef.Named("Cumflation")):false)
				{
					pawn.health.AddHediff(HediffDef.Named("RNW_BellyBulge"));
					pawn.Drawer.renderer.graphics.ClearCache();
					pawn.Drawer.renderer.graphics.ResolveAllGraphics();
					
				}
			}

			if (partner.health.hediffSet.HasHediff(HediffDef.Named("RNW_BellyBulge")))
			{
				partner.Drawer.renderer.graphics.ResolveAllGraphics();
				//partner.Drawer.renderer.graphics.ClearCache();
			}
			else
			{
				if (partner.IsVisiblyPregnant() || RimNudeWorldHARAddonMain.useCumflation ? partner.health.hediffSet.HasHediff(HediffDef.Named("Cumflation")) : false)
				{
					partner.health.AddHediff(HediffDef.Named("RNW_BellyBulge"));
					partner.Drawer.renderer.graphics.ClearCache();
					partner.Drawer.renderer.graphics.ResolveAllGraphics();
				}
			}
		}
	}

	[HarmonyBefore(new string[] { "rimworldanim" })]
	[HarmonyPatch(typeof(JobDriver_SexBaseInitiator), "End")]
	static class JobDriverEndPatch
	{
		public static void Postfix(JobDriver_SexBaseInitiator __instance)
		{
			__instance.pawn.Drawer.renderer.graphics.ClearCache();
			__instance.pawn.Drawer.renderer.graphics.ResolveAllGraphics();
			//PortraitsCache.SetDirty(__instance.pawn);


			(__instance.Target as Pawn)?.Drawer.renderer.graphics.ClearCache();
			(__instance.Target as Pawn)?.Drawer.renderer.graphics.ResolveAllGraphics();
			//PortraitsCache.SetDirty((__instance.Target as Pawn));
		}
	}
}